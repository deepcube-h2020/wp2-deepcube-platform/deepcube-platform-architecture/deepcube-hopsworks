# Hopsworks openstack infrastructure


## Packer: 
Create an image with Packer
- [Packer](https://www.packer.io/ "HashiCorp Packer's Homepage")
- [Openstack cli clients](https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html "Install Openstack command line clients") 
 - [jq](https://stedolan.github.io/jq/)

1. `cd 0.packer`
2. bash `source ../openrc.sh`
3. bash `source provider_ressources_ids.sh` 
4. `packer build -only=ubuntu-18-04 -var "image_output_name_ubuntu_18_04=Ubuntu 18.04 - GAEL" openstack.json`


## deepcube_wp2_network: 
Terraform private network provisioning.
This will create a private network and security groups.

1. `cd 1.deepcube_wp2_network`
2. bash `source ../openrc.sh`
3. `terraform init`
4. `terraform plan`
5. `terraform apply`

## deepcube_wp2_vms: 
This terraform code creates the virtual machines.

1. `cd 2.deepcube_wp2_vms`
2. bash `source ../openrc.sh`
3. `terraform init`
4. `terraform plan`
5. `terraform apply && ./scripts/build-inventory.sh`

## ansible: 
1. `cd 3.ansible`
2. `ansible-playbook -i ../2.deepcube_wp2_vms/inventory.cfg book_install_hopsworks.yml`
3. Go to `http://hopsworks_01_IP:9090/index.html#/terminal` and click `status` to track installation process
