# Terraform Settings
terraform {
  required_version = ">= v1.0.0"

  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
    }
  }

  # State storage
  backend "swift" {
    container         = "tfstate-deepcube-wp2-network"
    archive_container = "tfstate-deepcube-wp2-network-archive"
    region_name       = "GRA"
  }
}

# Private network
resource "openstack_networking_network_v2" "network_wp2_1" {
  name           = "network_wp2_1"
  admin_state_up = "true"
}

# Private subnet
resource "openstack_networking_subnet_v2" "subnet_wp2_1" {
  name       = "subnet_wp2_1"
  network_id = openstack_networking_network_v2.network_wp2_1.id
  cidr       = "192.168.1.0/24"
  enable_dhcp = true
  no_gateway = true
  ip_version = 4
  dns_nameservers = ["213.186.33.99", "8.8.8.8"]
}

# Private network
resource "openstack_networking_network_v2" "network_wp2_2" {
  name           = "network_wp2_2"
  admin_state_up = "true"
}

# Private subnet
resource "openstack_networking_subnet_v2" "subnet_wp2_2" {
  name       = "subnet_wp2_2"
  network_id = openstack_networking_network_v2.network_wp2_2.id
  cidr       = "192.168.2.0/24"
  enable_dhcp = true
  no_gateway = true
  ip_version = 4
  dns_nameservers = ["213.186.33.99", "8.8.8.8"]
}

# Security Group Http
resource "openstack_networking_secgroup_v2" "sg_http" {
  name        = "Http"
  description = "Allow http / https"
}

# Rule http
resource "openstack_networking_secgroup_rule_v2" "sg_rule_public_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.sg_http.id
}

# Rule https
resource "openstack_networking_secgroup_rule_v2" "sg_rule_public_https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.sg_http.id
}

# Security Group Http
resource "openstack_networking_secgroup_v2" "sg_ssh" {
  name        = "Ssh"
  description = "Allow ssh"
}

# Rule ssh
resource "openstack_networking_secgroup_rule_v2" "sg_rule_public_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.sg_ssh.id
}

# Security Group outbound traffic
resource "openstack_networking_secgroup_v2" "sec_egress_open" {
  name        = "Egress Open"
  description = "Allow outbound traffic"
}

resource "openstack_networking_secgroup_rule_v2" "out_all_ports" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.sec_egress_open.id
}

