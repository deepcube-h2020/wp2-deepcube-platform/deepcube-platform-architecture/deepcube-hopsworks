#cloud-config
# vim: syntax=yaml
# This is a cloud-config file for user-data

package_update: true
package_upgrade: true

manage_etc_hosts: false

write_files:
- path: /etc/netplan/50-cloud-init.yaml
  content: |
    network:
      version: 2
      ethernets:
        ens3:
          dhcp4: true
        ens4:
          dhcp4: true

runcmd:
- netplan --debug apply
- rm -f /etc/resolv.conf
- ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf

final_message: "The system is up, after $UPTIME seconds"
output:
  all: '| tee -a /var/log/cloud-init-output.log'
