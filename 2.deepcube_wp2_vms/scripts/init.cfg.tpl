#cloud-config
# vim: syntax=yaml
# This is a cloud-config file for user-data

# repo_update: true
# repo_upgrade: all
package_update: true
package_upgrade: true

final_message: "The system is up, after $UPTIME seconds"
output:
  all: '| tee -a /var/log/cloud-init-output.log'
