#!/bin/bash
set -ex 

echo "waiting for device..." > /tmp/cloud-init-script-wait.txt
while [ ! -f /tmp/remote-exec-finished.txt ]
do
  sleep 2
done

vgchange -ay

%{ for index, volume in volumes }

DEVICE_FS=`blkid -o value -s TYPE ${volume.device} || echo ""`
if [ "`echo -n $DEVICE_FS`" == "" ] ; then 
  # wait for the device to be attached
  DEVICENAME=`echo "${volume.device}" | awk -F '/' '{print $3}'`
  DEVICEEXISTS=''
  while [[ -z $DEVICEEXISTS ]]; do
    echo "checking $DEVICENAME"
    DEVICEEXISTS=`lsblk |grep "$DEVICENAME" |wc -l`
    if [[ $DEVICEEXISTS != "1" ]]; then
      sleep 10
    fi
  done
  pvcreate ${volume.device}
  vgcreate ${volume.lvm_vg} ${volume.device}
  lvcreate --name ${volume.lvm_lv} -l 100%FREE ${volume.lvm_vg}
  mkfs.ext4 /dev/${volume.lvm_vg}/${volume.lvm_lv}
fi
mkdir -p ${volume.mount_path}
echo "/dev/${volume.lvm_vg}/${volume.lvm_lv} ${volume.mount_path} ext4 defaults 0 0" >> /etc/fstab
mount ${volume.mount_path}

sleep 2

%{ endfor }
