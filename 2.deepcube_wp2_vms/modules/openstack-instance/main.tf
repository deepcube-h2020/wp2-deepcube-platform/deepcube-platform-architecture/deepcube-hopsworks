# Terraform Settings
terraform {
  required_version = ">= v1.0.0"

  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
    }
  }
}

provider "openstack" {
  auth_url    = "https://auth.cloud.ovh.net/v3"
  domain_name = "default" 
}

# Set default values
locals {
  flavor_name           = lookup(var.instances_defaults, "flavor_name", "d2-2")
  image_name            = lookup(var.instances_defaults, "image_name", "Debian 10")
  instance_prefix       = lookup(var.instances_defaults, "instance_prefix", "vm_")
  private_network_name  = lookup(var.instances_defaults, "private_network_name", "network_1")
  key_pair              = lookup(var.instances_defaults, "key_pair",  "default-keypair")
  ssh_private_key       = lookup(var.instances_defaults, "ssh_private_key", "~/.ssh/id_rsa")
  ssh_user              = lookup(var.instances_defaults, "ssh_user", "debian")
  security_groups       = lookup(var.instances_defaults, "security_groups", [ "default" ])
  volumes               = lookup(var.instances_defaults, "volumes", [])
  remote_exec           = lookup(var.instances_defaults, "remote_exec", [ "ls -l" ])
  user_data_file        = lookup(var.instances_defaults, "user_data_file", "scripts/init.cfg.tpl")
}

resource "openstack_compute_instance_v2" "openstack_instance" {
    count           = length(var.instances_specificity)
    name            = "${lookup(var.instances_specificity[count.index], "instance_prefix", local.instance_prefix)}-${format("%02d", count.index +1)}"
    flavor_name     = lookup(var.instances_specificity[count.index], "flavor_name", local.flavor_name)
    image_name      = lookup(var.instances_specificity[count.index], "image_name", local.image_name)
    key_pair        = lookup(var.instances_specificity[count.index], "key_pair", local.key_pair)
    security_groups = lookup(var.instances_specificity[count.index], "security_groups", local.security_groups)
    user_data       = element(data.template_cloudinit_config.my_userdata.*.rendered, count.index)
    
    # Public network
    network {
      name = "Ext-Net"
    }
    
    # Private network
    dynamic "network" {
      for_each = lookup(var.instances_specificity[count.index], "private_ip", "no_lan_ip") != "no_lan_ip" ? [ lookup(var.instances_specificity[count.index], "private_ip")] : []
      content {
        name        = lookup(var.instances_specificity[count.index], "private_network_name", local.private_network_name)
        fixed_ip_v4 = lookup(var.instances_specificity[count.index], "private_ip")
      }
    }

    # ssh connection informations
    connection {
      host        = self.access_ip_v4
      private_key = file(local.ssh_private_key)
      user        = lookup(var.instances_specificity[count.index], "ssh_user", local.ssh_user)
    }

    # ssh connect and exec command
    provisioner "remote-exec" {
      inline = lookup(var.instances_specificity[count.index], "remote_exec", local.remote_exec)
    }

    # Launch command locally
    provisioner "local-exec" {
      # Create inventory file for ansible
      interpreter = ["/bin/bash", "-c"]
      command = <<EOT
      echo -e "${self.name} ansible_host=${self.access_ip_v4} private_ip=${lookup(var.instances_specificity[count.index], "private_ip", "no_lan_ip")} ssh_user=${lookup(var.instances_specificity[count.index], "ssh_user", local.ssh_user)} ansible_ssh_private_key_file=${local.ssh_private_key}" | tee ${self.name}.ini;
      EOT
    }
}

resource "openstack_blockstorage_volume_v2" "volumes" {
  count       = length(var.instances_defaults.volumes) * length(var.instances_specificity)
  size        = var.instances_defaults.volumes[ count.index % length(var.instances_defaults.volumes) ].size
  name        = "${format("%02d", count.index + 1)}_${var.instances_defaults.volumes[ count.index % length(var.instances_defaults.volumes) ].prefix}"
  description = var.instances_defaults.volumes[ count.index % length(var.instances_defaults.volumes) ].description
  metadata    = {
    device = var.instances_defaults.volumes[ count.index % length(var.instances_defaults.volumes) ].device
    #readonly = "False" 
  } 
}

resource "openstack_compute_volume_attach_v2" "attachments" {
  count       = length(var.instances_defaults.volumes) * length(var.instances_specificity)
  volume_id   = openstack_blockstorage_volume_v2.volumes.*.id[ count.index ]
  instance_id = openstack_compute_instance_v2.openstack_instance.*.id[ floor(count.index / length(var.instances_defaults.volumes)) ]
  device      = openstack_blockstorage_volume_v2.volumes.*.metadata[count.index].device
  lifecycle {
    ignore_changes = [volume_id , instance_id ]
  }
}

data "template_cloudinit_config" "my_userdata" {
  count         = length(var.instances_specificity)
  gzip          = false
  base64_encode = false

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = templatefile(lookup(var.instances_specificity[count.index], "user_data_file", local.user_data_file), { })
  }
}
