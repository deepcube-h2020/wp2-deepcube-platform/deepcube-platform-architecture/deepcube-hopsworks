variable "instances_defaults" {
  type = any
  default = {}
}

variable "instances_specificity" {
  type = any
  default = [ {} ]
}
