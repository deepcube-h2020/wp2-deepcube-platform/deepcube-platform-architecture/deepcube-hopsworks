# Terraform Settings
terraform {
  required_version = ">= v1.0.0"

  backend "swift" {
    container         = "tfstate-deepcube-wp2"
    archive_container = "tfstate-deepcube-wp2-archive"
    region_name       = "GRA"
  }
  
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
    }
  }
}

# Hopsworks master instance
module "hopsworks_master" {
    source                  = "./modules/openstack-instance"

    instances_defaults = {
      flavor_name           = "d2-2"
      image_name            = "Ubuntu 18.04"
      key_pair              = "fb-keypair"
      ssh_private_key       = "~/.ssh/id_rsa"
      ssh_user              = "ubuntu"
      instance_prefix       = "hopsworks-master"
      private_network_name  = "network_wp2_1"
      security_groups       = [ "default" ]
      volumes               = [ ]
    }

    instances_specificity = [
      { 
        instance_prefix     = "hopsworks-master-fb"
        private_ip          = "192.168.1.130"
        user_data_file      = "scripts/init.cfg.ubuntu.tpl"
      }
    ]
}

# Hopsworks workers
module "hopsworks_workers" {
    source                  = "./modules/openstack-instance"

    instances_defaults = {
      flavor_name           = "d2-2"
      image_name            = "Ubuntu 18.04"
      key_pair              = "fb-keypair"
      ssh_private_key       = "~/.ssh/id_rsa"
      ssh_user              = "ubuntu"
      instance_prefix       = "hopsworks-worker-fb"
      private_network_name  = "network_wp2_1"
      security_groups       = [ "default" ]
      volumes               = [ ]
    }

    instances_specificity = [
      {
        flavor_name         = "d2-2"
        private_ip          = "192.168.1.131"
        user_data_file      = "scripts/init.cfg.ubuntu.tpl"
      }
      #,
      # { 
      #   private_ip          = "192.168.1.132"
      #   user_data_file      = "scripts/init.cfg.ubuntu.tpl"
      # }
    ]
}
