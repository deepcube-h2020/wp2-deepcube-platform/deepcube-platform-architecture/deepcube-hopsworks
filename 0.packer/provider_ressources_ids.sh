#!/usr/bin/env bash
# Get the id of the resources with openstack cli

FLAVOR_NAME="d2-2"
NETWORK_NAME="Ext-Net"
IMAGE_NAME_UBUNTU_18_04="Ubuntu 18.04"
IMAGE_NAME_UBUNTU_20_10="Ubuntu 20.10"
IMAGE_NAME_UBUNTU_21_04="Ubuntu 21.04"
IMAGE_NAME_DEBIAN_10="Debian 10"
IMAGE_NAME_DEBIAN_11="Debian 11"

# Flavor
echo "Getting flavor id for ${FLAVOR_NAME}..."
export FLAVOR_ID=`openstack flavor list -f json | jq -r ".[] | select(.Name == \"$FLAVOR_NAME\") | .ID"`
echo -e "FLAVOR_ID: $FLAVOR_ID \n"

# Network
echo "Getting network id for ${NETWORK_NAME}..."
export NETWORK_ID=`openstack network list -f json | jq -r ".[] | select(.Name == \"$NETWORK_NAME\") | .ID"`
echo -e "NETWORK_ID: $NETWORK_ID \n"

# OS
echo "Getting image ids..."
export IMAGE_ID_UBUNTU_18_04=`openstack image list -f json | jq -r ".[] | select(.Name == \"$IMAGE_NAME_UBUNTU_18_04\") | .ID"`
echo "IMAGE_ID_UBUNTU_18_04: $IMAGE_ID_UBUNTU_18_04"
export IMAGE_ID_UBUNTU_20_10=`openstack image list -f json | jq -r ".[] | select(.Name == \"$IMAGE_NAME_UBUNTU_20_10\") | .ID"`
echo "IMAGE_ID_UBUNTU_20_10: $IMAGE_ID_UBUNTU_20_10"
export IMAGE_ID_UBUNTU_21_04=`openstack image list -f json | jq -r ".[] | select(.Name == \"$IMAGE_NAME_UBUNTU_21_04\") | .ID"`
echo "IMAGE_ID_UBUNTU_21_04: $IMAGE_ID_UBUNTU_21_04"
export IMAGE_ID_DEBIAN_10=`openstack image list -f json | jq -r ".[] | select(.Name == \"$IMAGE_NAME_DEBIAN_10\") | .ID"`
echo "IMAGE_ID_DEBIAN_10: $IMAGE_ID_DEBIAN_10"
export IMAGE_ID_DEBIAN_11=`openstack image list -f json | jq -r ".[] | select(.Name == \"$IMAGE_NAME_DEBIAN_11\") | .ID"`
echo "IMAGE_ID_DEBIAN_11: $IMAGE_ID_DEBIAN_11"
